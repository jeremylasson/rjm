<?php
//Change the PHP script timeout to infinite
set_time_limit(0);

//Increase the memory limit
ini_set('memory_limit', '1024M');

//Hide undefined variable errors
error_reporting (E_ALL & ~E_NOTICE);
date_default_timezone_set('America/New_York');

require dirname(__FILE__) . '/functions.php';
require dirname(__FILE__) . '/../../lgcustomincludes/S3upload.php';
require dirname(__FILE__) . '/../../lgcustomincludes/Array2XML.class.php';
require dirname(__FILE__) . '/../../lgcustomincludes/henryV4.php';
$MASTER_PATH = dirname(__FILE__);
require dirname(__FILE__) . '/../../lgcustomincludes/datefiledelete.php';
require dirname(__FILE__) . '/../../lgcustomincludes/check_feed_integraty.php';
require dirname(__FILE__) . '/../../lgcustomincludes/global_supplier_location_update.php';
//check if tables created  update_item_locations($map = false,$msrp = false,$images =0,$cat=0  )
require dirname(__FILE__) . '/../../lgcustomincludes/create_default_supplier_tables.php';
require dirname(__FILE__) . '/../../lgcustomincludes/recordset_to_csv.php';



$sandbox = false;

date_default_timezone_set('America/New_York');

// Mail Reports To	
$mailTo = "jeremy@limitedgoods.com";
		$mailto=array();
		$cc=array();
		$bcc=array();
		array_push($mailto,array("jeremy@limitedgoods.com","Jeremy Lasson"));
$running_msg = array();

// Wholesaler ID

$feed1_size_sg = 50000;
$feed_columns = 49;		
//  Wholesaler ID
$wholesalerID = 129;
$SUPPLIER_NAME = "RJ MATHEWS";
$SUPPLIER_SHORT_NAME = "rjm";

## FTP SECTION ##
$ftp_host = 'ftp.gardnerinc.com';
$ftp_user = '702834@gardner'; 
$ftp_password = 'cmTt7BgC';

$ftp_dir ='/';



$ftp_filename= 'PRICING.CSV';
$ftp_filename_PNA = dirname(__FILE__) ."/../downloads/".$SUPPLIER_SHORT_NAME."feed-PNA.csv";





## DATABASE SECTION ##

// DATABASE SERVER
define('DB_SERVER', 'eitan.limitedgoods.com');

// DATABASE USERNAME
if ($sandbox) define('DB_SERVER_USERNAME', 'limit3_sandbox');
else define('DB_SERVER_USERNAME', 'limit3_server');


// DATABASE PASSWORD
if ($sandbox) define('DB_SERVER_PASSWORD', 'gothca123987!');
else define('DB_SERVER_PASSWORD', 'noamburt1');

// DATABASE DB NAME
if ($sandbox)define('DB_DATABASE', 'limit3_sandbox');
else define('DB_DATABASE', 'limit3');

// CONNECT TO DATABASE
$link = new mysqli(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD, DB_DATABASE);

//Set Character removed Non-printable i.e. á : "The character set should be understood and defined...(https://www.php.net/manual/en/mysqlinfo.concepts.charset.php)"
$link->set_charset('utf8');

if($link->connect_error){
    die("Connection Failed: ".$link->connect_error);
}echo "Connected Successful!";


//DB Products Tables (change for testing)
$products_table_name = 'products';
$location_table_name = 'products_item_location';
//$products_table_name = 'products_jeremytest';
//$location_table_name = 'products_item_location_jeremytest';
$categories_table_name = "products_".$SUPPLIER_SHORT_NAME."_item_cats";
$images_table_name = "products_".$SUPPLIER_SHORT_NAME."_image_links";
$map_table_name = "products_".$SUPPLIER_SHORT_NAME."_MAP_PRICE";
$msrp_table_name = "products_".$SUPPLIER_SHORT_NAME."_MSRP_PRICE";
$notes_table_name = "products_".$SUPPLIER_SHORT_NAME."_item_attirbutes";
//$item_qty_table_name = "products_".$SUPPLIER_SHORT_NAME."_item_qty";
$item_desc_table_name = "products_".$SUPPLIER_SHORT_NAME."_item_desc";


//check if tables created  update_item_locations($map = false,$msrp = false,$images =0,$cat=0  )
$MAP = true;
$MSRP = false;
create_default_supplier_tables($MAP,$MSRP ,1,0);


## S3 SECTION ##
$bucket_name = "lg-supplier-update-archive/".$SUPPLIER_SHORT_NAME; //Can be the root bucket name (cdn.limitedgoods.com) or a bucket name with folders (cdn.limitedgoods.com/ebayimages)
$access_policy = 'ACL_PRIVATE'; // Can be ACL_PRIVATE, ACL_PUBLIC_READ or ACL_PUBLIC_READ_WRITE



## API SECTION ##

// API CREDENTIALS
define('API_USER', '');
define('API_PASSWORD', '');


// API URL
define('APIURL', '');


## BUSINESS LOGIC ##

// BRANCH RANKING
$branchpriority = array();

//$branchpriority[1] = MD distribution center
//$branchpriority[2] = NV distribution center

$branchpriority[1][0] = 'Ohio';
$branchpriority[1][1] = 'Florida';

$branchpriority[2][0] = 'Florida';
$branchpriority[2][1] = 'Ohio';

// Minimum order amount
$minOrder = 1000;

// Ship to address
$ShipTo[1] = array(
      'ShipToName' => 'Limited Goods',
	  'ShipToAddress' => '7 Easter Court',
	  'ShipToAddress2' => 'STE A-B',
	  'ShipToCity' => 'Owings Mills',
	  'ShipToState' => 'MD',
	  'ShipToZip' => '21117'
);

$ShipTo[2] = array(
      'ShipToName' => 'Limited Goods',
	  'ShipToAddress' => '1410 Greg St',
	  'ShipToAddress2' => 'Suite 409',
	  'ShipToCity' => 'Sparks',
	  'ShipToState' => 'NV',
	  'ShipToZip' => '89431'
);

$ShipTo[3] = array(
      'ShipToName' => 'Limited Goods',
	  'ShipToAddress' => '4613 Parkway Dr',
	  'ShipToAddress2' => 'STE B',
	  'ShipToCity' => 'Texarkana',
	  'ShipToState' => 'AR',
	  'ShipToZip' => '71854'
);



//$DownloadURL = 'https://www.dropbox.com/sh/31vqnlkfm2irftp/AAD0rMmTGkhe5bw2eypmzL2oa/Product%20Data%20Files/Product-Feed.txt?dl=1';
//$DownloadURL2 = 'https://www.dropbox.com/sh/31vqnlkfm2irftp/AAAYnHzkeaoKv1R9PK85C1Cxa/Text%20Files/Inventory-Status.txt?dl=1';





/*Basic Item Details for Full Update*/
		$Basic_Item_Array = array(
		
		
			"VenderSKU"=>"",
			"Manufacturer"=>"",
			"ProdName"=>"",
			"ProdLongDesc"=>"",
			"ProdSKU"=>"",
			"UPC"=>"",
			"FK_ProdCatID"=>"",
			"FK_ConditionID"=>"",
			"ProdWeight"=>"",
			"ProdLength"=>"",
			"ProdWidth"=>"",
			"ProdHeight"=>"",
			"WholeSalePrice"=>"",
			"ProdQuantity"=>"",
			"image1"=>"",
			"WholeSaleCatName1"=>"",
			"WholeSaleCatName2"=>"",
			"MSRP"=>"",
			"MAP"=>"",

		
		
		
		);
$header  = array(
"Mfg_code","Part_number","Description","New_part_number","Sup_disp_code_(A=use_old_as_new)","(not_used)","(not_used)","(not_used)","List_price","Cost_price","(not_used)","(not_used)","(not_used)","Freight_price","Part_flag_(1=sub","2=sup","3=syn","5=nla)","Weight","(not_used)","(not_used)","(not_used)","(not_used)","Single_item_UPC_code","(not_used)","(not_used)","(not_used)","Vendor/UPC_pkg_size_2","Vendor/UPC_pkg_size_3","Vendor/UPC_pkg_size_4","Vendor/UPC_pkg_size_5","UPC_code_pkg_size_2","UPC_code_pkg_size_3","UPC_code_pkg_size_4","UPC_code_pkg_size_5","Each_dimension_width","Each_dimension_height","Each_dimension_depth","(not_used)","(not_used)","(not_used)","(not_used)","(not_used)","(not_used)","(not_used)","(not_used)","(not_used)","(not_used)","(not_used)","(not_used)","(not_used)","(not_used)"


);

//print_r($header );
//die();		
		
		$Basic_Item_Array_MAP = array(
		
		
			"VenderSKU"=>"",
			"Manufacturer"=>"",
			"ProdName"=>"Description",
			"ProdLongDesc"=>"",
			"ProdSKU"=>"Part_number",
			"UPC"=>"Single_item_UPC_code",
			"FK_ProdCatID"=>"",
			"FK_ConditionID"=>"",
			"ProdWeight"=>"",
			"ProdLength"=>"",
			"ProdWidth"=>"",
			"ProdHeight"=>"",
			"ProdQuantity"=>"",
			"WholeSalePrice"=>"Cost_price",
			"image1"=>'',
			"WholeSaleCatName1"=>'',
			"WholeSaleCatName2"=>'',
			"MSRP"=>'List_price',
			"Mfg_code"=>'Mfg_code',
			"MAP"=>'',

		
		
		
		);

		// Specific for this Supplier
		$Custom_Feed_Array = array("Freight_price"=>"",);
		
		ksort($Custom_Feed_Array);
//asort($Custom_Feed_Array, SORT_NATURAL | SORT_FLAG_CASE);
		$Custom_Feed_Cols  = implodeArrayKeys($Custom_Feed_Array);  
//`ProdSKU` varchar(50) default NULL

		//echo ($Custom_Feed_Cols);
		//die();

		$Insert_Array = array_merge($Basic_Item_Array,$Custom_Feed_Array);
//look up for full manu name from the code Mfg_code
$MANU_LU = array(
				 "AGR"=>"AGRI-FAB",
				"B3C"=>"B3C PRODUCT - FUEL CLEANER",
				"CP"=>"CHAMPION SPARK PLUGS",
				"DR"=>"DR POWER",
				"GEN"=>"GENERAC",
				"HG"=>"HYDRO GEAR",
				"HOP"=>"HUSQVARNA",
				"KH"=>"KOHLER ENGINES",
				"NG"=>"SPARK PLUGS",
				"OR"=>"OREGON",
				"PFD"=>"PFERD INC.",
				"SI"=>"OREGON",
				"TEC"=>"TECUMSEH ENGINES",
				"TRU"=>"TRU 50 FUEL QUART/CASES",
				"WL"=>"WALBRO CARBURETORS",
				"ZA"=>"ZAMA CARBURETORS",

);
?>
