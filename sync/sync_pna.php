<?php
 //Define location of required files in baltauto directory
require (dirname(__FILE__) . '/../includes/config.php');


//Change the PHP script timeout to infinite
set_time_limit(0);

//Increase the memory limit
ini_set('memory_limit', '-1');

$running_log = '';
$running_msg = array();
$newproducts = 0;
$updatedproducts = 0;
$lines = 0;
$query = array();
$progress = 0;
$error = 0;

$temp_table_name = $SUPPLIER_SHORT_NAME."_pna_temp";


$time_start = microtime(true);

$running_log .= "\nStarting...";
array_push($running_msg, "Starting...");
$time_start = microtime(true);

$running_log .= "\nStarting...";

$running_log .= "\nDownloading from FTP: $ftp_host";

// Download inventory file from FTP
$local_file = $ftp_filename_PNA;


//This function will kill the script if the feed file hasn't been updated since the last sync
fileUpdated2($ftp_filename_PNA, 2);




$running_log .= "\nCreating file object from download.";

$feedObject = createFileObject($local_file,",",1);




$running_log .= "\nCreating file object from download.";
array_push($running_msg, "Creating file object from download ");

while (!$feedObject->eof()) { 
    $lines++; 
    $feedObject->fgetcsv(); 
}

$lines--;

$running_log .= "\nThe file object contains " . $lines . " items.";
/**/
if ($lines > 1) {
    $running_log .= "\nThe file object was successfully created.  Uploading feed file to S3 for archiving";
    
    if (SendToS3($local_file, $bucket_name, $access_policy)) {
        $running_log .= "\nFile successfully copied to S3 {$bucket_name}/".baseName($local_file).PHP_EOL;
    } else {
        $running_log .= "\nFailed to copy the file to S3!";
    }
} else {
    $running_log .= "\nThere was a problem creating the file object!";
       syncFatalError(2, $running_log);

}


//Drop the temp table
$dropTempQuery = "DROP TABLE IF EXISTS " . $temp_table_name;
if (!mysqli_query($link, $dropTempQuery)) { //Drop table failed.  Stop the update.
    $running_log .= "\nFailed to drop the TEMP table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    
       syncFatalError(2, $running_log);

} 


//Create the temp table
$createTempTableQuery = "CREATE TABLE IF NOT EXISTS " . $temp_table_name . " (
                    `ProdID` int(11) NOT NULL auto_increment,
                    `VenderSKU` varchar(50) NOT NULL default '',
                    `ProdQuantity` int(11) NOT NULL default '0',
                    `LOC1` int(11) NOT NULL default '0',
					`is_updated` int(1) default 0,
                    PRIMARY KEY  (`ProdID`),KEY `gardner_pna_temp_idx1` USING BTREE (`VenderSKU`)
                  );
";

if (!mysqli_query($link, $createTempTableQuery)) {
    $running_log .= "\nFailed to create the TEMP table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    
       syncFatalError(2, $running_log);

} else {
    $running_log .= "\nTEMP table was successfully created.";
}

//populate temp table from 
$createTempTableQuery = "INSERT INTO " . $temp_table_name . " (VenderSKU,ProdQuantity,LOC1) (SELECT ItemNumber, ReadyToShip, ReadyToShip FROM limit3_sandbox.RJMatthews_Products)";

if (!mysqli_query($link, $createTempTableQuery)) {
    $running_log .= "\nFailed to populate the TEMP table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
    
       syncFatalError(2, $running_log);

} else {
    $running_log .= "\nTEMP table was successfully populate.";
}


//TESTING ; when testing this is good to uncomment to kill the script right here after all data is imported into the temp table
die($running_log);
				//Set the flag for updates
				$startQuery = "UPDATE " . $products_table_name . " SET LOCK_UPDATE_QUAN = 1 WHERE WholeSaler = " . $wholesalerID;

				if (!mysqli_query($link, $startQuery)) {
					//This is a non-fatal error.  Log and continue
					$running_log .= "\nFailed to set the update flag.";
					$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
					$error = 1;
					
					if (mysqli_errno($link) == "1205") {
						$running_log .= "\nThe products table is currently locked.  Will try again later";
						   syncFatalError(2, $running_log);
        
					}
					
					   syncFatalError(2, $running_log);
  
				} else {
					$flag_count = mysqli_affected_rows($link);
					$running_log .= "\nSuccessfully set the update flag on for " . $flag_count . " products.";
				}
//Update Item locations table from the temp table
/*                	*/
$countLocationQuery = "SELECT * FROM " . $location_table_name . " WHERE FK_WholesalerID = " . $wholesalerID;
if (!mysqli_query($link, $countLocationQuery)){
    $running_log .= "\nFailed to select the number of products in the locations table.";
    $running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
        
       syncFatalError(2, $running_log);

} else {
    $location_count = mysqli_affected_rows($link);
}
    
					$deleteLocationsQuery = "DELETE FROM " . $location_table_name . " WHERE FK_WholesalerID = " . $wholesalerID;
					if (!mysqli_query($link, $deleteLocationsQuery)) {
						$running_log .= "\nFailed to delete from the product locations table.";
						$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
							
						   syncFatalError(2, $running_log);

					} else {
						$delete_count = mysqli_affected_rows($link);
							
						if ($location_count != $delete_count) {
							$running_log .= "\nThe number of rows deleted from the locations table does not equal what should have been deleted.  Something happened.";
							$running_log .= "\nThere were " . $location_count . " items in the location table but only " . $delete_count . " items were deleted.";
							   syncFatalError(2, $running_log);

						} else {
							$running_log .= "\nSuccessfully deleted the old product locations.";
						}
							
					}
						
					$updateLocationsQuery = "INSERT INTO " . $location_table_name . " (VenderSKU,FK_WholesalerID,LOC1) (SELECT VenderSKU, " . $wholesalerID . ", LOC1  FROM " . $temp_table_name . ")";
					if (!mysqli_query($link, $updateLocationsQuery)){
						$running_log .= "\nFailed to update the locations table.";
						$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
						  
						   syncFatalError(2, $running_log);

					} else {
						$running_log .= "\nSuccessfully updated the current product locations.";
					}
				
					//Update the prices from the temp table
					$updatePriceQuery = "UPDATE " . $products_table_name . " p, " . $temp_table_name . " t SET /* p.WholeSalePrice = t.WholeSalePrice, */ p.LOCK_DISC = 0, p.LOCK_UPDATE_QUAN = 0, p.Active = 1 WHERE p.VenderSKU = t.VenderSKU  AND p.WholeSaler = " . $wholesalerID . " AND p.LOCK_UPDATE_PRICE = 0";
					if (!mysqli_query($link, $updatePriceQuery)){
						$running_log .= "\nFailed to update the product prices.";
						$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
						
						if (mysqli_errno($link) == "1205") {
							$running_log .= "\nThe products table is currently locked.  Will try again later";
							   syncFatalError(2, $running_log);
        
						}
						   syncFatalError(2, $running_log);

					} else {
						$running_log .= "\nSuccessfully updated the product prices.";
						$update_count = mysqli_affected_rows($link);
					}
					
					//Update FK_ProdID in Locations Table
					/*	*/$updateLocationProdIDQuery = "UPDATE " . $location_table_name . " l JOIN " . $products_table_name . " p on (l.VenderSKU = p.VenderSKU AND l.FK_WholesalerID = p.WholeSaler) SET l.FK_ProdID = p.ProdID  WHERE p.WholeSaler = " . $wholesalerID;
					if (!mysqli_query($link, $updateLocationProdIDQuery)) {
						//This is a non-fatal error.  Log and continue
						$running_log .= "\nFailed to update the FK_ProdID in the Locations table.";
						$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
						$error = 1;
					} else {
						$count_location_prod = mysqli_affected_rows($link);
						$running_log .= "\nSuccessfully updated the FK_ProdID for " . $count_location_prod . " products in the Locations table.";    
					}
				$running_log .= "\n\nUpdating product quantity";  
			
                               
                                
				//Update the product details from temp table
				
				$updateDetailQuery = "UPDATE " . $products_table_name . " p, " . $temp_table_name . " t SET p.ProdQuantity = t.ProdQuantity, p.TimeStamp = NOW(), LOCK_UPDATE_QUAN = 0 WHERE p.VenderSKU = t.VenderSKU  AND p.WholeSaler = ".$wholesalerID;
				if (!mysqli_query($link, $updateDetailQuery)) {
					$running_log .= "\nFailed to update the product details and quantities.";
					$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
						
					if (mysqli_errno($link) == "1205") {
						$running_log .= "\nThe products table is currently locked.  Will try again later";
						   syncFatalError(2, $running_log);
        
					}
						
					   syncFatalError(2, $running_log);

				} else {
					$running_log .= "\nSuccessfully updated the product details and quantities.";
					$update_count = mysqli_affected_rows($link);        
				}
				//Any products that are still locked were missing in the csv file, set them as discontinued
				if ($error == 0) {
					$discontinueQuery = "UPDATE " . $products_table_name . " SET LOCK_DISC = 1, LOCK_UPDATE_QUAN = 0, ProdQuantity = 0, Active = 0 WHERE LOCK_UPDATE_QUAN = 1 AND WholeSaler = " . $wholesalerID;
					if (!mysqli_query($link, $discontinueQuery)) {
						//This is a non-fatal error.  Log and continue
						$running_log .= "\nFailed to set products to discontinued.";
						$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
						$error = 1;
					} else {
						$discproducts = mysqli_affected_rows($link);
						$running_log .= "\nSuccessfully set " . $discproducts . " products as discontinued.";
					}
				} else {
					$running_log .= "\nDidn't set any products to discontinued as a safeguard because errors were encountered.";
					$discproducts = 0;
				}		

/*LG QTY SYNCED ITEMS*/

					$updateLocationProdIDQuery = "UPDATE `products_lg_stock_parent` a JOIN   ".$products_table_name." p on a.`LG_ProdID` = p.`ProdID` SET a.`LG_ProdQuantity` = p.`ProdQuantity`;";

					if (!mysqli_query($link, $updateLocationProdIDQuery)) {
						//This is a non-fatal error.  Log and continue
						$running_log .= "\nFailed to update LG QTY SYNCED ITEMS QTY.";
						$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
						$error = 1;
					} else {
						$count_location_prod = mysqli_affected_rows($link);
						$running_log .= "\nSuccessfully updated the LG QTY SYNCED ITEMS " . $count_location_prod;    
					}
					$updateLocationProdIDQuery = "UPDATE `products_lg_stock_parent` a 
					JOIN  ".$products_table_name."  p on a.`Sup_VenderSKU` = p.`VenderSKU`
					SET p.`ProdQuantity` = a.`LG_ProdQuantity`, p.LOCK_DISC = 0 
					WHERE a.`FK_WholesalerID` =  ".$wholesalerID." AND p.`WholeSaler` =  ".$wholesalerID." AND a.`Sync_ProdQuantity` = 1 AND a.`LG_ProdQuantity` > 0;";

					if (!mysqli_query($link, $updateLocationProdIDQuery)) {
						//This is a non-fatal error.  Log and continue
						$running_log .= "\nFailed to update LG QTY SYNCED ITEMS.";
						$running_log .= "\nThe SQL error was " . mysqli_error($link) . ".";
						$error = 1;
					} else {
						$count_location_prod = mysqli_affected_rows($link);
						$running_log .= "\nSuccessfully updated the LG QTY SYNCED ITEMS " . $count_location_prod;    
					}
/*END LG QTY SYNCED ITEMS*/				
$time_end = microtime(true);
$execution_time = ($time_end - $time_start)/60;

$running_log .= "\n\n\nTotal Execution Time: ".$execution_time." Mins";
$running_log .= "\nProcessed " . $lines . " products \n";
$running_log .= "\nThere were " . $update_count . " products updated and " . $newproducts . " products added.\n";
$running_log .= "\nThere were " . $discproducts . " products marked as discontinued.\n";


//Send Email on success
			$message = "$SUPPLIER_NAME products PNA sync script finished successfully.  The script log is below:\n".$running_log;
			$subject ="$SUPPLIER_NAME PNA Sync Script Completed Successfully";
			Henry_Email($subject, $message,$mailto,$cc,$bcc,false,true);
$feedObject = NULL;

$running_log .= "\nCleaning up downloads...";
unlink($local_file);

//DEBUG
echo $running_log;

mysqli_close($link);
?>
